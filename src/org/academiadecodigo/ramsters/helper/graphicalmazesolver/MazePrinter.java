package org.academiadecodigo.ramsters.helper.graphicalmazesolver;

import org.academiadecodigo.ramsters.helper.maze.Vector2;
import org.academiadecodigo.ramsters.helper.maze.tree.Node;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.LinkedList;
import java.util.Queue;

public class MazePrinter {

    private int squareSize;

    private int canvasLength;
    private int canvasWidth;

    private int numberOfColumns;
    private int numberOfRows;

    public MazePrinter(int squareSize, int numberOfColumns, int numberOfRows) {

        this.squareSize = squareSize;
        this.numberOfColumns = numberOfColumns;
        this.numberOfRows = numberOfRows;

        setCanvasDimensions();
    }

    public void setCanvasDimensions() {
        canvasLength = squareSize * numberOfColumns;
        canvasWidth = squareSize * numberOfRows;
    }

    /**
     * Creates our canvas and paints the squares of our maze without being connected to each other
     */

    public void init() {
        Rectangle canvasBackGroundRectangle = new Rectangle(0, 0, canvasLength, canvasWidth);
        canvasBackGroundRectangle.setColor(Color.BLACK);
        canvasBackGroundRectangle.fill();

        printSquares();
    }

    private void printSquares() {

        int squareHalfSize = squareSize / 2;
        int squareQuarterSize = squareSize / 4;

        int initialSquarePositionX = squareQuarterSize;
        int initialSquarePositionY = squareQuarterSize;

        int currentSquarePositionX = initialSquarePositionX;
        int currentSquarePositionY = initialSquarePositionY;

        for (int row = 0; row < numberOfRows; row++) {

            for (int column = 0; column < numberOfColumns; column++) {

                Rectangle square = new Rectangle(currentSquarePositionX, currentSquarePositionY, squareHalfSize, squareHalfSize);

                square.setColor(Color.WHITE);
                square.fill();

                currentSquarePositionX += squareSize;
            }

            currentSquarePositionX = initialSquarePositionX;
            currentSquarePositionY += squareSize;
        }
    }

    public void printSquare(Vector2 position, Color color){

        int halfSquareSize = squareSize / 2;
        int quarterSquareSize = squareSize / 4;

        Rectangle square = new Rectangle(position.getX() * squareSize + quarterSquareSize, position.getY() * squareSize + quarterSquareSize, halfSquareSize, halfSquareSize);

        square.setColor(color);
        square.fill();
    }

    private Vector2 connectionPosition(Vector2 parentPosition, Vector2 childPosition) {

        int squareQuarterSize = squareSize / 4;

        int parentX = parentPosition.getX();
        int parentY = parentPosition.getY();

        int childX = childPosition.getX();
        int childY = childPosition.getY();

        if (parentX < childX) {

            return new Vector2(squareQuarterSize * 3 + parentX * squareSize, squareQuarterSize + parentY * squareSize);
        } else if (parentX > childX) {

            return new Vector2(squareQuarterSize * 3 + childX * squareSize , squareQuarterSize + childY * squareSize);
        } else {

            if (parentY < childY) {

                return new Vector2(squareQuarterSize + parentX * squareSize, squareQuarterSize * 3 + parentY * squareSize);
            } else {

                return new Vector2(squareQuarterSize + childX * squareSize, squareQuarterSize * 3 + childY * squareSize);
            }
        }
    }

    private void printConnection(Vector2 position){
        printConnection(position, Color.WHITE);
    }

    private void printConnection(Vector2 position, Color color) {

        int halfSquareSize = squareSize / 2;

        Rectangle connection = new Rectangle( position.getX(), position.getY(), halfSquareSize, halfSquareSize);

        connection.setColor(color);
        connection.fill();
    }

    public void printConnection(Vector2 parentPosition, Vector2 childPosition, Color color){
        Vector2 connectionPosition = connectionPosition(parentPosition, childPosition);

        printConnection(connectionPosition, color);
    }

    // breadth first search
    public void printMaze(Node root, int delay) throws InterruptedException{

        Queue<Node> nodeQueue = new LinkedList<>();

        nodeQueue.add(root);

        while (nodeQueue.size() > 0) {

            for (Node child : nodeQueue.poll().getChildren()) {

                Vector2 parentPosition = child.getParent().getCoordinates();

                Vector2 connectionPos = connectionPosition(parentPosition, child.getCoordinates());

                printConnection(connectionPos);

                nodeQueue.add(child);

                Thread.sleep(delay);
            }
        }
    }
}
