package org.academiadecodigo.ramsters.helper.terminalmazesolver;

import org.academiadecodigo.ramsters.helper.maze.AnsiColor;
import org.academiadecodigo.ramsters.helper.maze.Vector2;
import org.academiadecodigo.ramsters.helper.maze.tree.Node;

import java.util.Stack;

public class MazeSolver {
    private String[][] mazeStringArray;
    private Node root;
    private Node exit;

    public MazeSolver(String[][] mazeStringArray, Node root, Node exit) {
        this.mazeStringArray = mazeStringArray;
        this.root = root;
        this.exit = exit;
    }

    public void start(Stack<Node> correctPath) {
        depthSearchTreeCoordinatesAndPrintMaze(root, correctPath, exit.getCoordinates());
    }

    public Node depthSearchTreeCoordinatesAndPrintMaze(Node root, Stack<Node> correctPath, Vector2 coordinatesToFind) {

        if (correctPath.peek() == root) {
            correctPath.pop();

            if (root.getCoordinates().equals(this.root.getCoordinates())) {
                changeMazeStringArrayBackGroundColorAtPosition(root.getCoordinates(), AnsiColor.BLUE_BACKGROUND);
            } else if (root.getCoordinates().equals(this.exit.getCoordinates())) {
                changeMazeStringArrayBackGroundColorAtPosition(root.getCoordinates(), AnsiColor.BLUE_BACKGROUND);
            } else {
                changeMazeStringArrayBackGroundColorAtPosition(root.getCoordinates(), AnsiColor.GREEN_BACKGROUND);
            }

            MazePrinter.PrintMazeStringArray(mazeStringArray, this.root, exit);
        }

        if (root.getCoordinates().equals(coordinatesToFind)) {
            return root;
        }

        for (Node child : root.getChildren()) {
            Node correctNode = depthSearchTreeCoordinatesAndPrintMaze(child, correctPath, coordinatesToFind);
            if (correctNode != null) {
                return correctNode;
            }
        }

        return null;
    }

    public void changeMazeStringArrayBackGroundColorAtPosition(Vector2 position, AnsiColor color) {
        String mazeStringArrayAtPosition = mazeStringArray[position.getX()][position.getY()];
        if (mazeStringArrayAtPosition.charAt(1) == '|') {
            mazeStringArray[position.getX()][position.getY()] = color.getAnsiColor() + mazeStringArrayAtPosition.charAt(0) + AnsiColor.ANSI_RESET.getAnsiColor() + mazeStringArrayAtPosition.charAt(1);
        } else {
            mazeStringArray[position.getX()][position.getY()] = color.getAnsiColor() + mazeStringArrayAtPosition + AnsiColor.ANSI_RESET.getAnsiColor();
        }
    }
}


