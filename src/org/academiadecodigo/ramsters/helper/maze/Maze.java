package org.academiadecodigo.ramsters.helper.maze;

import org.academiadecodigo.ramsters.helper.maze.tree.Node;
import org.academiadecodigo.ramsters.helper.maze.tree.Tree;

import java.util.*;

// we consider it to start at (0,0) and go up to (mazeX-1, mazeY-1)
public class Maze {

    // x and y must be smaller than mazeX and mazeY respectively
    private int mazeSizeX, mazeSizeY;

    // coordinates of our entrance to the mazeTree
    private Vector2 startingCoordinates;

    // the coordinates we are currently looking at when generating the mazeTree
    private Vector2 currentCoordinates;

    private Stack<Node> longestPath;

    private Node entrance;
    private Node exit;


    public Tree mazeTree;

    public Maze(int mazeSizeX, int mazeSizeY) {
        this.mazeSizeX = mazeSizeX;
        this.mazeSizeY = mazeSizeY;
        startingCoordinates = new Vector2(0, 0);
        currentCoordinates = startingCoordinates;
        mazeTree = new Tree();
    }

    public Node getEntrance(){
        return entrance;
    }

    public Node getExit(){
        return exit;
    }

    public Stack<Node> getLongestPath(){
        return (Stack<Node>) longestPath.clone();
    }

    public void generateMaze() {

        // Create a way to go back on previous nodes already created
        Stack<Node> nodeGenerationStack = new Stack<Node>();

        nodeGenerationStack.push(mazeTree.getRoot());

        while(nodeGenerationStack.size() > 0){
            generateMazePath(nodeGenerationStack);
        }

        longestPath = Tree.searchLongestPath(mazeTree.getRoot());

        entrance = longestPath.lastElement();
        exit = longestPath.firstElement();
    }


    // This first mazeTree path algorithm does not take into consideration stopping unless it has no other place to go
    private void generateMazePath(Stack<Node> nodeGenerationStack) {


        boolean finishedGenerating = false;

        while (!finishedGenerating) {
            Set<Direction> acceptableDirections = nextGenerationDirection();

            if (acceptableDirections.size() > 0) {
                Direction nextDirection = randomizeNextDirection(acceptableDirections);

                currentCoordinates = getUnitVector(nextDirection);

                Node child = new Node(currentCoordinates, new ArrayList<>());
                Node lastNodeInStack = nodeGenerationStack.peek();
                child.setParent(lastNodeInStack);
                lastNodeInStack.getChildren().add(child);

                nodeGenerationStack.push(child);
            } else {
                // The current node can no longer generate new paths
                nodeGenerationStack.pop();
                // If we haven't gone back to the root, reassign currentCoordinates so we can keep looking for new paths to generate
                if(nodeGenerationStack.size() > 0){
                    currentCoordinates = nodeGenerationStack.peek().getCoordinates();
                }
                finishedGenerating = true;
            }
        }
    }

    private boolean canGenerateUp(){
        return (currentCoordinates.getY() < mazeSizeY - 1);
    }

    private boolean canGenerateDown(){
        return currentCoordinates.getY() > 0;
    }

    private boolean canGenerateLeft(){
        return currentCoordinates.getX() > 0;
    }

    private boolean canGenerateRight(){
        return (currentCoordinates.getX() < mazeSizeX - 1);
    }

    // tells us if it is possible to keep generating paths on the mazeTree from the currentCoordinates
    // returns a dictionary with booleans saying if we can go "up", "down", "left" or "right"
    private Set nextGenerationDirection() {

        Map<Direction, Boolean> acceptableDirections = new HashMap<>() {{
            put(Direction.UP, false);
            put(Direction.DOWN, false);
            put(Direction.LEFT, false);
            put(Direction.RIGHT, false);
        }};

        if (canGenerateLeft()) {
            Vector2 coordinatesToTheLeft = Vector2.add(currentCoordinates, new Vector2(-1, 0));

            // check if have already generated to the left
            if (Tree.depthSearchTreeCoordinates(coordinatesToTheLeft, mazeTree.getRoot()) == null) { // there aren't any obstructions to the left
                // we can go left
                acceptableDirections.put(Direction.LEFT, true);
            }
        }

        if (canGenerateDown()) {

            Vector2 coordinatesDown = Vector2.add(currentCoordinates, new Vector2(0, -1));

            if (Tree.depthSearchTreeCoordinates(coordinatesDown, mazeTree.getRoot()) == null) { // there aren't any obstructions going down
                // we can go down
                acceptableDirections.put(Direction.DOWN, true);
            }
        }

        if (canGenerateRight()) {

            Vector2 coordinatesToTheRight = Vector2.add(currentCoordinates, new Vector2(1, 0));

            if (Tree.depthSearchTreeCoordinates(coordinatesToTheRight, mazeTree.getRoot()) == null) {
                acceptableDirections.put(Direction.RIGHT, true);
            }
        }

        if (canGenerateUp()) {

            Vector2 coordinatesUp = Vector2.add(currentCoordinates, new Vector2(0, 1));

            if (Tree.depthSearchTreeCoordinates(coordinatesUp, mazeTree.getRoot()) == null) {
                acceptableDirections.put(Direction.UP, true);
            }
        }

        // trim down all the directions to only the ones that we can use
        Set<Direction> availableDirections = new HashSet<>();

        for(Map.Entry<Direction, Boolean> entry : acceptableDirections.entrySet()){
            Direction direction = entry.getKey();
            Boolean isAcceptable = entry.getValue();

            if(isAcceptable){
                availableDirections.add(direction);
            }
        }

        return availableDirections;
    }

    private Direction randomizeNextDirection(Set<Direction> acceptableDirections) {

        // decide which direction will be changed to get the next coordinates
        int numberOfDirections = acceptableDirections.size();

        Random random = new Random();
        int randomValue = random.nextInt(numberOfDirections);

        // set is not an order collection
        // we iterate over it to find out which is our random element

        int cont = 0;

        for (Direction direction : acceptableDirections) {

            if (cont == randomValue) {
                return direction;
            }
            else {
                cont++;
            }
        }

        // should'nt get here
        return null;
    }

    // direction can either be "up", "down", "left" or "right"
    private Vector2 getUnitVector(Direction direction) {

        Map<Direction, Vector2> directionVector = new HashMap<>() {{
            put(Direction.UP, new Vector2(0, 1));
            put(Direction.DOWN, new Vector2(0, -1));
            put(Direction.LEFT, new Vector2(-1, 0));
            put(Direction.RIGHT, new Vector2(1, 0));
        }};

        return Vector2.add(currentCoordinates, directionVector.get(direction));
    }
}