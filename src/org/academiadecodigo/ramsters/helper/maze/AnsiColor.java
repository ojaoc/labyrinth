package org.academiadecodigo.ramsters.helper.maze;

public enum AnsiColor {
    GREEN_BACKGROUND("\u001B[42m"),
    RED_BACKGROUND("\u001B[41m"),
    BLUE_BACKGROUND("\u001B[44m"),
    ANSI_RESET("\u001B[0m");

    private String ansiColor;

    AnsiColor(String ansiColor){
        this.ansiColor = ansiColor;
    }

    public String getAnsiColor() {
        return ansiColor;
    }
}
