package org.academiadecodigo.ramsters.helper.maze;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
