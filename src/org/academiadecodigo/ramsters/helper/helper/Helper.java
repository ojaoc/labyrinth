package org.academiadecodigo.ramsters.helper.helper;

import java.util.Random;

public class Helper {

    // inclusive to both parameters
    public static int getRandomIntInInterval(int minRange, int maxRange){
        Random random = new Random();

        // +1 because they are both inclusive
        int difference = maxRange - minRange + 1;
        int numberInInterval = random.nextInt(difference) + minRange;

        return numberInInterval;
    }

    public static boolean binarySearch(int[] array, int number, int leftPointer, int rightPointer){
        if(number < array[leftPointer] || number > array[rightPointer]){
            return false;
        }

        int pointerDifference = rightPointer - leftPointer;
        int middlePointer = pointerDifference / 2 + leftPointer;

        if(array[middlePointer] == number){
            return true;
        }

        if(number < array[middlePointer]) {
            return binarySearch(array, number, 0, middlePointer - 1);
        }else{ // number is greater than the middle pointer
            return binarySearch(array, number, middlePointer + 1, rightPointer);
        }
    }
}
